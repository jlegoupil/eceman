#include <stdio.h>
#include <stdlib.h>

int lire_fichier(char *filename, char *data_fichier, int size)
{

    ///declaration variable

    FILE * fic = NULL;
    int ficlenght,nb_char;


    ///Ouverture du fichier
    fic=fopen(filename,"r");
    ///deplacer fin du fichier
    fseek(fic,0,SEEK_END);
    ///affectation taille du fichier
    ficlenght=ftell(fic);
    rewind(fic);

    ///lecture fichier
    if (ficlenght<size){

        nb_char=fread(data_fichier,(size_t)sizeof(char),(size_t)ficlenght, fic);

    }
    else
        printf("Erreur fichier trop gros");
    ///fermeture fichier
    fclose(fic);
    return nb_char;
}
